// soal 1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

var gabungKata = kataPertama.concat(" ", kataKedua.charAt(0).toUpperCase() + kataKedua.slice(1), " ", kataKetiga, " ", kataKeempat.toUpperCase());
console.log('.:: Soal 1 ::.');
console.log(gabungKata);

// soal 2
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

var jumlahValue = parseInt(kataPertama) + parseInt(kataKedua) + parseInt(kataKetiga) + parseInt(kataKeempat);
console.log('.:: Soal 2 ::.');
console.log(jumlahValue);

// soal 3
var kalimat = 'wah javascript itu keren sekali'; 
var kalimatArrray = kalimat.split(" ");

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimatArrray[1]; // do your own! 
var kataKetiga = kalimatArrray[2]; // do your own! 
var kataKeempat = kalimatArrray[3]; // do your own! 
var kataKelima  = kalimatArrray[4]; // do your own! 

console.log('.:: Soal 3 ::.');
console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

// soal 4
var nilai = 75;
var indeks = "";
if (nilai >= 80) {
    indeks = "A";
} else if (nilai >= 70 && nilai < 80) {
    indeks = "B";
} else if (nilai >= 60 && nilai < 70) {
    indeks = "c";
} else if (nilai >= 50 && nilai < 60) {
    indeks = "D";
} else if (nilai < 50) {
    indeks = "E";
}
console.log('.:: Soal 4 ::.');
console.log(indeks);

// soal 5
var tanggal = 03;
var bulan = 3;
var tahun = 1991;

var bulanString = "";
switch (bulan) {
    case 1: bulanString = "Januari"; break;
    case 2: bulanString = "Februari"; break;
    case 3: bulanString = "Maret"; break;
    case 4: bulanString = "April"; break;
    case 5: bulanString = "Mei"; break;
    case 6: bulanString = "Juni"; break;
    case 7: bulanString = "Juli"; break;
    case 8: bulanString = "Agustus"; break;
    case 9: bulanString = "September"; break;
    case 10: bulanString = "Oktober"; break;
    case 11: bulanString = "November"; break;
    case 12: bulanString = "Desember"; break;
}

var value = "";
if (bulan < 1 && bulan > 12) {
    value = "Bulan tidak diketahui, harap isi bulan dengan benar";
} else {
    value = tanggal.toString().concat(" ", bulanString, " ", tahun);
}
console.log('.:: Soal 5 ::.');
console.log(value);