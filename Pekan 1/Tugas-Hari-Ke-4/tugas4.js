// soal 1
console.log(".:: Soal 1 ::.");
var start_number = 1;
console.log('LOOPING PERTAMA');
while (start_number <= 20) {
    if (start_number % 2 == 0) {
        console.log(start_number + ' - I love coding');
    }
    start_number++;
}

var start_number = 20;
console.log('LOOPING KEDUA');
while (start_number > 0) {
    if (start_number % 2 == 0) {
        console.log(start_number + ' - I will become a frontend developer');
    }
    start_number--;
}

// soal 2
console.log("");
console.log(".:: Soal 2 ::.");
for (var i=1; i<=20; i++) {
    if ((i % 3 == 0) && (i % 2 != 0)) {
        console.log(i + ' - I Love Coding');
    } else {
        if (i % 2 != 0) {
            console.log(i + ' - Santai');
        } else if (i % 2 == 0) {
            console.log(i + ' - Berkualitas');
        }
    }
}

// soal 3
console.log("");
console.log(".:: Soal 3 ::.");
var start_number = 1;
while (start_number <= 7) {
    var string = "";
    for (var i=0; i<start_number; i++) {
        string += "#";
    }
    console.log(string);

    start_number++;
}

// soal 4
console.log("");
console.log(".:: Soal 4 ::.");
var kalimat="saya sangat senang belajar javascript"
console.log(kalimat.split(" "));

// soal 5
console.log("");
console.log(".:: Soal 5 ::.");
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
daftarBuah.sort();
daftarBuah.forEach(record => {
    console.log(record);
});