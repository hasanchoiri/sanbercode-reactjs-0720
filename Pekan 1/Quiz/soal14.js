function tampilkanBintang(jumlahBintang) {
    var bintang = "";
    while (jumlahBintang > 0) {
        var string = "";
        for (var i=0; i<jumlahBintang; i++) {
            string += "#";
        }
        bintang += string + '\n';
        jumlahBintang--;
    }

    return bintang;
}

console.log(tampilkanBintang(15));