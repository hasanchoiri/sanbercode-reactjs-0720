// fungsi menghitung luas lingkaran
function luasLingkaran(jari2) {
    jari2 = parseFloat(jari2);
    if (isNaN(jari2)) {
        jari2 = 0.0
    }

    var luas = 3.14*jari2*jari2;
    return luas;
}

// fungsi menghitung luas segitiga
function luasSegitiga(alas, tinggi) {
    var luas = (alas*tinggi)*0.5;
    return luas;
}

// fungsi menghitung sama persegi
function luasPersegi(sisi) {
    var luas = sisi * sisi;
    return luas;
}

console.log(luasPersegi(14));