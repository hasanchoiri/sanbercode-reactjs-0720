// soal 1
luasLingkaran = (jari2) => {
    jari2 = parseFloat(jari2);
    if (isNaN(jari2)) {
        jari2 = 0.0
    }

    const luas = 3.14*jari2*jari2;
    return luas;
}

kelilingLingkaran = (jari2) => {
    jari2 = parseFloat(jari2);
    if (isNaN(jari2)) {
        jari2 = 0.0
    }

    const keliling = 3.14*(jari2+jari2);
    return keliling;
}
console.log(".:: Soal 1 ::.");
let jari2 = 9;
console.log('Luas Lingkaran: '+ luasLingkaran(jari2));
console.log('Keliling Lingkaran: '+kelilingLingkaran(jari2));

// soal 2
let kalimat = "";
addKalimat = (var1,var2,var3,var4,var5) => {
    const theString = `${var1} ${var2} ${var3} ${var4} ${var5}`;
    return theString;
}
console.log(".:: Soal 2 ::.");
console.log(addKalimat("saya","adalah","seorang","frontend","developer"));

// soal 3
class Book {
    constructor() {
        this._name = "";
        this._totalPage = 0;
        this._price = "";
    }
    set name(value) {
        this._name = value;
    }
    set totalPage(value) {
        this._totalPage = value;
    }
    set price(value) {
        this._price = value;
    }
}

class Komik extends Book {
    constructor() {
        super();
        this._isColorful = false;
    }
    detailBuku() {
        if (this._price > 5000) {
            this._isColorful = true;
        }

        let detail = {
            "name": this._name,
            "totalPage": this._totalPage,
            "price": this._price,
            "isColorFul": this._isColorful
        }

        return detail;
    }
}

console.log(".:: Soal 3 ::.");
komik = new Komik();
komik.name = "One Piece";
komik.totalPage = 20;
komik.price = 10000;
console.log(komik.detailBuku());

komik = new Komik();
komik.name = "Naruto";
komik.totalPage = 50;
komik.price = 1000;
console.log(komik.detailBuku());