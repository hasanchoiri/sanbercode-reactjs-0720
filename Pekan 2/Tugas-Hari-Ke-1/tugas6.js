// soal 1
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992]
var objectDaftarPeserta = {
    nama: "Asep",
    jenisKelamin: "laki-laki",
    hobi: "baca buku",
    tahunLahir: 1992
}
console.log(".:: Soal 1 ::.")
console.log(objectDaftarPeserta)

// soal 2
var arrayOfObject = [{
    nama: "strawberry",
    warna: "merah",
    adaBijinya: "tidak",
    harga: 9000 
},{
    nama: "jeruk",
    warna: "oranye",
    adaBijinya: "ada",
    harga: 8000
},{
    nama: "Semangka",
    warna: "Hijau & Merah",
    adaBijinya: "ada",
    harga: 10000
},{
    nama: "Pisang",
    warna: "Kuning",
    adaBijinya: "tidak",
    harga: 5000
}]
console.log(".:: Soal 2 ::.")
console.log(arrayOfObject[0])

// soal 3
var dataFilm = []
function addDataFilm(item = []) {
    dataFilm.push(item)

    return dataFilm
}
addDataFilm({
    nama: 'Marvel Film',
    durasi: '2 jam',
    genre: 'Hero Action',
    tahun: 2019
})
addDataFilm({
    nama: 'One Piece',
    durasi: '5 menit',
    genre: 'Manga Anime',
    tahun: 2020
})
console.log(".:: Soal 3 ::.")
console.debug(dataFilm)

// soal 4
// Release 0
class Animal {
    // Code class di sini
    constructor(name) {
        this._name = name
        this._band = 4
        this._legs = 4
        this._coldBlooded = false
    }
    get name() {
        return this._name
    }
    get legs() {
        return this._legs
    }
    get coldBlooded() {
        return this._coldBlooded
    }
    set legs(val) {
        this._legs = val
    }
}
 
var sheep = new Animal("shaun")
 
console.log(".:: Soal 4 ::.")
console.log("=> Release 0")
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.coldBlooded) // false

class Ape extends Animal {
    constructor(name) {
        super(name)
    }
    yell() {
        return "Auooo"
    }
}

class Frog extends Animal {
    constructor(name) {
        super(name)
    }
    jump() {
        return "hop hop"
    }
}
console.log("=> Release 1")
var sungokong = new Ape("kera sakti")
sungokong.legs = 2
console.log(sungokong.name) // "shaun"
console.log(sungokong.legs) // 2
console.log(sungokong.coldBlooded) // false
console.log(sungokong.yell()) // "Auooo"

var kodok = new Frog("buduk")
console.log(kodok.name) // "shaun"
console.log(kodok.legs) // 4
console.log(kodok.coldBlooded) // false
console.log(kodok.jump()) // "hop hop" 

// soal 5
class Clock {
    
    constructor ({ template }) {
        this._timer
        this._template = template
    }

    render() {
        var date = new Date()
  
        var hours = date.getHours()
        if (hours < 10) hours = '0' + hours
    
        var mins = date.getMinutes()
        if (mins < 10) mins = '0' + mins
    
        var secs = date.getSeconds()
        if (secs < 10) secs = '0' + secs
    
        var output = this._template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs)
    
        console.log(output)
    }
  
    stop() {
        clearInterval(this._timer)
    }
  
    start() {
        var self = this;
        self._timer = setInterval(function() { self.render() }, 1000);
    }
  
  }
  
  console.log(".:: Soal 5 ::.")
  var clock = new Clock({template: 'h:m:s'})
  clock.start()