import React from 'react';
import './App.css';
import './css/style.css';

function App() {
  return (
    <div className="App">
      <section className="section" style={{ border: "1px solid #bdbdbd", padding: 20 }}>
        <h1>Form Pembelian Buah</h1>
        <div className="label ">
          <form action="">
            <table border="0" style={{ "margin-left": 50 }}>
              <tr>
                <td width={150} align="left">Nama Pelanggan</td>
                <td><input type="text" name="nama" /></td>
              </tr>
              <tr>
                <td valign="bottom" align="left">Daftar Item</td>
                <td align="left">
                  <div>
                    <input type="checkbox" name="buah" value="semangka" />
                    <label for="scales">Semangka</label>
                  </div>
                  <div>
                    <input type="checkbox" name="buah" value="jeruk" />
                    <label for="scales">Jeruk</label>
                  </div>
                  <div>
                    <input type="checkbox" name="buah" value="nanas" />
                    <label for="scales">Nanas</label>
                  </div>
                  <div>
                    <input type="checkbox" name="buah" value="salak" />
                    <label for="scales">Salak</label>
                  </div>
                  <div>
                    <input type="checkbox" name="buah" value="anggur" />
                    <label for="scales">Anggur</label>
                  </div>
                </td>
              </tr>
              <tr>
                <td align="left"><input type="submit" value="Kirim" /></td>
              </tr>
            </table>
          </form>
        </div>
      </section>
      {/* <header className="App-header">
        
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header> */}
    </div>
  );
}

export default App;
